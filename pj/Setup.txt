(Instructions for Windows 7 x64)

1. Install Anaconda.
2. Create virtual environment called "nerve"
    conda create --name nerve python=2
3. Activate the virtual environment "nerve"
    activate nerve
4. Install numpy and jupyter
    conda install numpy jupyter
5. Install opencv
    conda install -c https://conda.binstar.org/menpo opencv
6. Open a jupyter notebook
    jupyter notebook
7. After done, stop the notebook session
    CTRL+C
8. Deactivate the virtual environment
    deactivate